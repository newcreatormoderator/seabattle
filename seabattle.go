package main

import (
    "fmt"
    "strings"
    "os"
    "time"
    "math/rand"
    "encoding/json"
    "io/ioutil"
    "log"
    "net/http"
    "github.com/gin-gonic/gin"

    _ "github.com/mattn/go-sqlite3"

    "database/sql"
    "strconv"
    "bytes"
    )


type Config struct {
    MaxY, MaxX  int
    MinY,  MinX int
    Ships ships
}

type ships struct {
    Oneranked, Tworanked    int
    Threeranked, Fourranked int
}

type dataDB struct {
    Users users
    Gamestate gamestate
    Statistics userstat
}

type users struct {
    ID int
    Username string
}

type gamestate struct {
    ID int
    User_ID int
    Completed bool
    Mapstate string
    Shots string
}

type userstat struct {
    User_ID int
    Games_Total int
    Games_Won int
}

type fullstat struct {
    SimpleField [][]string `json:"simpleField"`
    SecretField [][]int    `json:"secretField"`
    Stats	stats      `json:"stats"`
    Shoots	[]shoots   `json:"shoots"`
    LuckShoots	[]shoots   `json:"luckshoots"`
}

type stats struct {
    Ships_remain    int `json:"ships_remain"`
    Ships_destroyed int `json:"ships_destroyed"`
    Shots_total     int `json:"shots_total"`
    Shots_hit       int `json:"shots_hit"`
    Shots_miss      int `json:"shots_miss"`
}

type shoots struct {
    X int `json:"X"`
    Y int `json:"Y"`
}

var FSData fullstat
var DataBaseSB dataDB 
var GlobalConn *sql.DB
var GlobalErr error

func moreless(min, max, a int) bool {
    return  a > max || a < min
}

func samecoordinates(x, y, basedX, basedY, rank int) bool {
    for j := -1; j < 2; j++{
	for i := -1; i < rank+1; i++{
	    if (x+i == basedX && y+j == basedY) || (x+j == basedX && y+i == basedY){
		return true
	    } 
	}
    }
    return false
}

func openConfFileJson() Config {
    content, err := ioutil.ReadFile("./config.json")
    if err != nil {
	log.Fatal("Error when opening file: ", err)
    }
    var conf Config
    err = json.Unmarshal(content, &conf)
    if err != nil {
        log.Fatal("Error duringUnmarshal(): ", err)
    }
    return conf
}

func makeSimplefield(x, y int) [][]string {
    field := make([][]string,y)
    for i:= range field{
	field[i] = make([]string,x)
    }
    for i:= 0; i < y; i++{
	for j:= 0; j < x; j++{
	    field[i][j] = "+"
	}
    }
    return field
}

func makeSecretfield(x, y int) [][]int {
    field := make([][]int ,y)
    for i:= range field{
	field[i] = make([]int ,x)
    }
    return field
}

func makeships(summ, level, x, y int, CoordX, CoordY []int) ([]int, []int) {
    var parX, parY, isY int
    for i:= 0; i < summ; i++ {
	rand.Seed(time.Now().UnixNano())
	Xparam := 21
	Yparam := 21
	isY = rand.Intn(2)
	if isY == 1 {
	    parX = rand.Intn(x)
	    parY = rand.Intn(y-level)
	} else {
	    parX = rand.Intn(x-level)
	    parY = rand.Intn(y)
	}
	for z:= 0; z < len(CoordX); z++ {
	    if samecoordinates(parX, parY, CoordX[z], CoordY[z], level) {
		i -= 1
		Xparam = CoordX[z]
		Yparam = CoordY[z]
		break
	    }
	}
	if samecoordinates(parX, parY, Xparam, Yparam, level) == false {
	for j:= 0; j < level; j++ {
	    if isY == 1 {
		CoordX = append(CoordX, parX)
		CoordY = append(CoordY, parY+j)
	    } else {
		CoordX = append(CoordX, parX+j)
		CoordY = append(CoordY, parY)
	    }
	}
	}
    }
    return CoordX, CoordY
}

func putships(field [][]int, coordX, coordY []int) [][]int {
    for i:= 0; i < len(coordX); i++{
	field[coordY[i]][coordX[i]] = 1
    }
    return field
}

func SecretToString(field [][]int) string{
    
    var buffer bytes.Buffer
    for i := 0; i < len(field); i++ {
	for j := 0; j < len(field[i]); j++{
	    buffer.WriteString(strconv.Itoa(field[i][j]))
	    if (j == len(field[i])-1) && (i < len(field)-1){
		buffer.WriteString(", ")
	    }
	}
    }

    return buffer.String()
}

func ShootsToString(field []shoots) string{
    
    var buffer bytes.Buffer
    for i := 0; i < len(field); i++ {
	buffer.WriteString(strconv.Itoa(field[i].X)+".")
	buffer.WriteString(strconv.Itoa(field[i].Y)+", ")
    }
    return buffer.String()
}

func turnRepeat(x, y int, Shot []shoots) bool{
    for i := 0; i < len(Shot); i++{
	if x == Shot[i].X && y == Shot[i].Y{
	    return true
	}
    }
    return false
}

func changeStat(x, y int, simpleF [][]string, secretF [][]int) [][]string{
    switch secretF[y][x]{
	case 0: 
    simpleF[y][x] = "x"
	case 1:
    simpleF[y][x] = "*"
    }
    return simpleF
}

func getMap(c *gin.Context){
    c.JSON(http.StatusOK, FSData.SimpleField)
}

func postShoot(c *gin.Context){
    var newShoot shoots
    var descode bool

    if err := c.BindJSON(&newShoot); err != nil {
	return
    }
    if moreless(0, len(FSData.SecretField[0])-1, newShoot.X) || moreless(0, len(FSData.SecretField)-1, newShoot.Y){
	c.JSON(http.StatusBadRequest, gin.H{"message": "Invalid coordinates"})
	return
    }
    if turnRepeat(newShoot.X, newShoot.Y, FSData.Shoots){
	c.JSON(http.StatusBadRequest, gin.H{"message": "You have already done this turn!"})
	return
    } else {
	descode = destroyed(newShoot.X, newShoot.Y, FSData.SimpleField, FSData.SecretField)
        FSData.SimpleField = PlayGame(newShoot.X, newShoot.Y, FSData.SimpleField, FSData.SecretField, descode)
        descode = destroyed(newShoot.X, newShoot.Y, FSData.SimpleField, FSData.SecretField)
        FSData.SimpleField = PlayGame(newShoot.X, newShoot.Y, FSData.SimpleField, FSData.SecretField, descode)
	FSData.Shoots = append(FSData.Shoots, newShoot)
	FSData.Stats.Shots_total = len(FSData.Shoots)
	if DataBaseSB.Gamestate.Shots == "" {
	    DataBaseSB.Gamestate.Shots = strconv.Itoa(newShoot.X)+"."+strconv.Itoa(newShoot.Y)
	} else {
	    DataBaseSB.Gamestate.Shots = DataBaseSB.Gamestate.Shots+", "+strconv.Itoa(newShoot.X)+"."+strconv.Itoa(newShoot.Y)
	}
	fmt.Println(DataBaseSB.Gamestate.Shots)

	postShots := "UPDATE gamestate SET shots = \""+DataBaseSB.Gamestate.Shots+"\" WHERE id = "+strconv.Itoa(DataBaseSB.Gamestate.ID)
	fmt.Println(postShots)
	_, GlobalErr = GlobalConn.Exec(postShots)
	if GlobalErr != nil {
	    panic(GlobalErr)
	}
    }
    if FSData.SecretField[newShoot.Y][newShoot.X] == 1 {
        c.JSON(http.StatusOK, gin.H{"message": "hit!"})
        FSData.LuckShoots = append(FSData.LuckShoots, newShoot)
	FSData.Stats.Shots_hit = len(FSData.LuckShoots)
    } else {
	c.JSON(http.StatusOK, gin.H{"message": "Miss!"})
	FSData.Stats.Shots_miss = len(FSData.Shoots)-len(FSData.LuckShoots)
    }
    if destroyed(newShoot.X, newShoot.Y, FSData.SimpleField, FSData.SecretField){
	FSData.Stats.Ships_destroyed += 1
	FSData.Stats.Ships_remain -= 1
	c.JSON(http.StatusOK, gin.H{"message": "Destroyed!"})
    }
    c.JSON(http.StatusCreated, newShoot)
    statsfile, _ := json.MarshalIndent(FSData, "", " ")
    _ = ioutil.WriteFile("gamestate.json", statsfile, 0644)
    if FSData.Stats.Ships_remain == 0 {
	postEnd := "UPDATE gamestate SET completed = 1 WHERE id = "+strconv.Itoa(DataBaseSB.Gamestate.ID)
	getGW := "SELECT games_won FROM statistics WHERE user_id = "+strconv.Itoa(DataBaseSB.Users.ID)
	GlobalErr = GlobalConn.QueryRow(getGW).Scan(&DataBaseSB.Statistics.Games_Won)
	if GlobalErr != nil {
	    panic(GlobalErr)
	}
	_, GlobalErr = GlobalConn.Exec(postEnd)
	if GlobalErr != nil {
	    panic(GlobalErr)
	}
	postGamesWon := "UPDATE statistics SET games_won = "+strconv.Itoa(DataBaseSB.Statistics.Games_Won+1)+" WHERE user_id = "+strconv.Itoa(DataBaseSB.Users.ID)
	_, GlobalErr = GlobalConn.Exec(postGamesWon)
	c.JSON(http.StatusOK, gin.H{"message": "Victory!"})
    }
}

func getCount(c *gin.Context){
    c.IndentedJSON(http.StatusOK, FSData.Stats.Shots_total)
}

func getStats(c *gin.Context){
    c.IndentedJSON(http.StatusOK, FSData.Stats)
}

func getSecret(c *gin.Context){
    c.JSON(http.StatusOK, FSData.SecretField)
}

func PlayGame(x, y int, simpleF [][]string, secretF [][]int, destr bool) [][]string {
    if destr{
	fpointX := 0
	fpointY := 0
	rangX := 0
	rangY := 0
	for i := -1; ; i--{
	    if x+i >= 0{
		if simpleF[y][x+i] == "*"{
		    fpointX -= 1	
		} else {
		    break
		}
	    } else { break }
	}
	for i := -1; ; i--{
	    if y+i >= 0{
		if simpleF[y+i][x] == "*"{
		    fpointY -= 1	
		} else {
		    break
		}
	    } else { break }
	}
	for i := 1; ; i++{
	    if x+i < len(secretF){
		if simpleF[y][x+i] == "*"{
		    rangX += 1	
		} else {
		    break
		}
	    } else { break }
	}
	for i := 1; ; i++{
	    if y+i < len(secretF){
		if simpleF[y+i][x] == "*"{
		    rangY += 1	
		} else {
		    break
		}
	    } else { break }
	}
	for i := fpointX-1; i < rangX+2; i++{
	    if x+i < 0 {
		i += 1	
	    }
	    if x+i >= len(secretF[0]){
		break
	    }
	    for j := fpointY-1; j < rangY+2; j++{
		if y+j < 0{
		    j += 1	
		}
		if y+j < len(secretF){
		    simpleF = changeStat(x+i, y+j, simpleF, secretF)
		}
	    }
	}    
    } else {
	simpleF = changeStat(x, y, simpleF, secretF)
    }
    return simpleF
}

func destroyed(x, y int, simpleF [][]string, secretF [][]int) bool{
    if secretF[y][x] == 0 {
	return false
    } else {
	fpointX := 0
	fpointY := 0
	rangX := 0
	rangY := 0
	for i := -1; ; i--{
	    if x+i >= 0{ 
		if secretF[y][x+i] == 1{
		    fpointX -= 1
		} else {
		    break
		}
	    }
	    if x+i < 0{ break }
	}
	for i := -1; ; i--{
	    if y+i >= 0{
		if secretF[y+i][x] == 1{
		    fpointY -= 1
		} else {
		    break
		}
	    }
	    if y+i < 0{ break }
	}
	for i := 1; ; i++{
	    if x+i < len(secretF[0]){
		if secretF[y][x+i] == 1{
		    rangX += 1
		} else {
		    break
		}
	    } else { break } 
	}
	for i := 1; ; i++{
	    if y+i < len(secretF){
		if secretF[y+i][x] == 1{
		    rangY += 1
		} else { break }
	    }
	    if y+i >= len(secretF){ break }
	}
	for i := fpointX; i < rangX+1; i++ {
	    if x+i < 0{
		i += 1
		fmt.Printf("%d\n", x+i)
	    }
	    if x+i >= len(secretF[0]){
		break
	    }
	    for j := fpointY; j < rangY+1; j++ {
		if y+j < 0{
		    j += 1	
		    fmt.Printf("%d\n", y+j)
	    	}
	    	if y+j < len(secretF){
		    if simpleF[y+j][x+i] == "+" && secretF[y+j][x+i] == 1 {
			return false
		    }
		}
	    }
	}
    }
    return true
}

func NewGame(Conf Config) fullstat {
    var countX, countY int
    var CoordinatesX, CoordinatesY []int
    fmt.Print("Enter a field's size Y,X (ex. 10,10): ")
    fmt.Scanf("%d,%d\n", &countY, &countX)
    if moreless(Conf.MinX, Conf.MaxX, countX) || moreless(Conf.MinY, Conf.MaxY, countY) {
	fmt.Println("Invalid size parameters.")
	os.Exit(1)
    }
    FSData.Stats.Ships_remain = Conf.Ships.Fourranked + Conf.Ships.Threeranked + Conf.Ships.Tworanked + Conf.Ships.Oneranked
    FSData.SecretField = makeSecretfield(countX, countY)
    FSData.SimpleField = makeSimplefield(countX, countY)
    CoordinatesX, CoordinatesY = makeships(Conf.Ships.Fourranked, 4, countX, countY, CoordinatesX, CoordinatesY)
    CoordinatesX, CoordinatesY = makeships(Conf.Ships.Threeranked, 3, countX, countY, CoordinatesX, CoordinatesY)
    CoordinatesX, CoordinatesY = makeships(Conf.Ships.Tworanked, 2, countX, countY, CoordinatesX, CoordinatesY)
    CoordinatesX, CoordinatesY = makeships(Conf.Ships.Oneranked, 1, countX, countY, CoordinatesX, CoordinatesY)
    FSData.SecretField = putships(FSData.SecretField, CoordinatesX, CoordinatesY)
    statsfile, _ := json.MarshalIndent(FSData, "", " ")
    _ = ioutil.WriteFile("web-service-gin/gamestate.json", statsfile, 0644)
    return FSData
}

func ContinueGame(GameID string, ShipsC int) fullstat{
    var DataFromStat fullstat
    var BufMap, StShots []string
    var StStArMap [][]string
    var descode bool
    var UnmodShoots shoots
    DataFromStat.Stats.Ships_remain = ShipsC
    fmt.Println(DataFromStat.Stats.Ships_remain)
    getContinue := "SELECT mapstate, shots FROM gamestate WHERE id = "+GameID
    GlobalErr = GlobalConn.QueryRow(getContinue).Scan(&DataBaseSB.Gamestate.Mapstate, &DataBaseSB.Gamestate.Shots)
    if GlobalErr != nil {
	panic(GlobalErr)
    }
    StArMap := strings.Split(DataBaseSB.Gamestate.Mapstate, ", ")
    for i:= 0; i < len(StArMap); i++ {
	BufMap = strings.Split(StArMap[i], "")
	StStArMap = append(StStArMap, BufMap)
    }
    DataFromStat.SecretField = makeSecretfield(len(StStArMap), len(StStArMap[0]))
    for i := 0; i < len(StStArMap); i++ {
	for j := 0; j < len(StStArMap[i]); j++ {
	    DataFromStat.SecretField[i][j], GlobalErr = strconv.Atoi(StStArMap[i][j])
	}
    }
    if DataBaseSB.Gamestate.Shots != "" {
	StArShots := strings.Split(DataBaseSB.Gamestate.Shots, ", ")
	for i := 0; i < len(StArShots); i++ {
	    StShots = strings.Split(StArShots[i], ".")
	    UnmodShoots.X, GlobalErr = strconv.Atoi(StShots[0])
	    UnmodShoots.Y, GlobalErr = strconv.Atoi(StShots[1])
	    DataFromStat.Shoots = append(DataFromStat.Shoots, UnmodShoots)
	}
    }
    DataFromStat.SimpleField = makeSimplefield(len(DataFromStat.SecretField), len(DataFromStat.SecretField[0]))
    for i := 0; i < len(DataFromStat.Shoots); i++ {
	descode = destroyed(DataFromStat.Shoots[i].X, DataFromStat.Shoots[i].Y, DataFromStat.SimpleField, DataFromStat.SecretField)
	DataFromStat.SimpleField = PlayGame(DataFromStat.Shoots[i].X, DataFromStat.Shoots[i].Y, DataFromStat.SimpleField, DataFromStat.SecretField, descode)
	descode = destroyed(DataFromStat.Shoots[i].X, DataFromStat.Shoots[i].Y, DataFromStat.SimpleField, DataFromStat.SecretField)
	DataFromStat.SimpleField = PlayGame(DataFromStat.Shoots[i].X, DataFromStat.Shoots[i].Y, DataFromStat.SimpleField, DataFromStat.SecretField, descode)
	DataFromStat.Stats.Shots_total += 1
	if DataFromStat.SecretField[DataFromStat.Shoots[i].Y][DataFromStat.Shoots[i].X] == 1 {
	    DataFromStat.LuckShoots = append(DataFromStat.LuckShoots, DataFromStat.Shoots[i])
	    DataFromStat.Stats.Shots_hit = len(DataFromStat.LuckShoots)
	}
	DataFromStat.Stats.Shots_miss = DataFromStat.Stats.Shots_total - DataFromStat.Stats.Shots_hit
	if descode{
	    DataFromStat.Stats.Ships_destroyed += 1
	    DataFromStat.Stats.Ships_remain -= 1
	}
    }
    return DataFromStat
}

func main() {
    var Conf Config
    var answer, username string
    var GIDarray []int
    var UsCount, GSCount, GID int
    Conf = openConfFileJson()
    GlobalConn, GlobalErr = sql.Open("sqlite3", "./sb.db")
    if GlobalErr != nil {
	panic(GlobalErr)
    }
    GetTable := `CREATE TABLE IF NOT EXISTS users(id INTEGER, username TEXT);
    CREATE TABLE IF NOT EXISTS gamestate(id INTEGER, user_id INTEGER, completed INTEGER, mapstate TEXT, shots TEXT);
    CREATE TABLE IF NOT EXISTS statistics(user_id INTEGER, games_total INTEGER, games_won INTEGER)`
    _, GlobalErr = GlobalConn.Exec(GetTable)
    defer GlobalConn.Close()
    for username = ""; len(username) < 1; {
	fmt.Print("Enter your name: ")
	fmt.Scanf("%s\n", &username)
	if username == ""{
	    fmt.Println("Empty string unaccepted!")
	}
    }
    GetUID := "SELECT count(*) FROM users";
    GlobalErr = GlobalConn.QueryRow(GetUID).Scan(&UsCount);
    ShipsCount := Conf.Ships.Fourranked + Conf.Ships.Threeranked + Conf.Ships.Tworanked + Conf.Ships.Oneranked
    if UsCount == 0{
	_, GlobalErr = GlobalConn.Exec("INSERT INTO users VALUES ("+strconv.Itoa(UsCount+1)+", \""+username+"\")")
	FSData = NewGame(Conf)
	getGSCount := "SELECT count(*) FROM gamestate"
	_, GlobalErr = GlobalConn.Exec("INSERT INTO statistics VALUES("+strconv.Itoa(UsCount+1)+", 1, 0)");
	if GlobalErr != nil {
	    panic(GlobalErr)
	}
	DataBaseSB.Users.ID = UsCount+1
	DataBaseSB.Gamestate.ID = GSCount+1
	GlobalErr = GlobalConn.QueryRow(getGSCount).Scan(&GSCount);
	stGameID := strconv.Itoa(GSCount+1)
	stSecret := SecretToString(FSData.SecretField)
	_, GlobalErr = GlobalConn.Exec("INSERT INTO gamestate VALUES("+stGameID+", "+strconv.Itoa(UsCount+1)+", 0, \""+stSecret+"\", \"\")");
	if GlobalErr != nil {
	    panic(GlobalErr)
	}
    }
    for i := 1; i < UsCount+1; i++{
	stI := strconv.Itoa(i)
	queryUSERNAME := "SELECT username FROM users WHERE id = "+stI
	GlobalErr = GlobalConn.QueryRow(queryUSERNAME).Scan(&DataBaseSB.Users.Username)
	if GlobalErr != nil {
	    panic(GlobalErr)
	}
	if username == DataBaseSB.Users.Username{
	    DataBaseSB.Users.ID = i
	    stUID := strconv.Itoa(DataBaseSB.Users.ID)
	    getGID := "SELECT id FROM gamestate WHERE user_id = "+stUID+" AND completed = 0"
	    row, err := GlobalConn.Query(getGID)
	    for row.Next() {
		err = row.Scan(&GID);
		if err != nil {
		    panic(err)
		}
		GIDarray = append(GIDarray, GID) 
	    }
	    if len(GIDarray) > 0 {
		fmt.Print("Do you want start a new game? y/n: ")
		fmt.Scanf("%s", &answer)
	    } else {
		answer = "Y"
	    }
	    getUID := "SELECT id FROM users WHERE username = \""+username+"\""
	    GlobalErr = GlobalConn.QueryRow(getUID).Scan(&DataBaseSB.Users.ID);
	    if GlobalErr != nil {
		panic(GlobalErr)
	    }
 	    if answer == "y" || answer == "Y"{
		FSData = NewGame(Conf)
		getGSCount := "SELECT count(*) FROM gamestate"
		GlobalErr = GlobalConn.QueryRow(getGSCount).Scan(&GSCount);
		fmt.Println("SELECT games_total FROM statistics WHERE user_id = "+strconv.Itoa(DataBaseSB.Users.ID))
		GlobalErr = GlobalConn.QueryRow("SELECT games_total FROM statistics WHERE user_id = "+strconv.Itoa(DataBaseSB.Users.ID)).Scan(&DataBaseSB.Statistics.Games_Total);
		postTotal := "UPDATE statistics SET games_total = "+strconv.Itoa(DataBaseSB.Statistics.Games_Total+1)+" WHERE user_id = "+strconv.Itoa(DataBaseSB.Users.ID)
		_, GlobalErr = GlobalConn.Exec(postTotal);
		DataBaseSB.Gamestate.ID = GSCount+1
		stGameID := strconv.Itoa(DataBaseSB.Gamestate.ID)
		stSecret := SecretToString(FSData.SecretField)
		_, GlobalErr = GlobalConn.Exec("INSERT INTO gamestate VALUES("+stGameID+", "+stUID+", 0, \""+stSecret+"\", \"\")");
		if GlobalErr != nil {
		    panic(GlobalErr)
		}
	    } else {
		fmt.Printf("your unended games: %v\n", GIDarray)
		fmt.Print("Enter game ID: ")
		fmt.Scanf("%d", &DataBaseSB.Gamestate.ID)
		FSData = ContinueGame(strconv.Itoa(DataBaseSB.Gamestate.ID), ShipsCount)
	    }
	    break
	}
	if (i == UsCount) && (username != DataBaseSB.Users.Username) {
	    DataBaseSB.Users.ID = UsCount+1
	    DataBaseSB.Gamestate.ID = GSCount+1
	    _, GlobalErr = GlobalConn.Exec("INSERT INTO users VALUES ("+strconv.Itoa(DataBaseSB.Users.ID)+", \""+username+"\")")
	    FSData = NewGame(Conf)
	    getGSCount := "SELECT count(*) FROM gamestate"
	    GlobalErr = GlobalConn.QueryRow(getGSCount).Scan(&GSCount);
	    DataBaseSB.Gamestate.ID = GSCount+1
	    _, GlobalErr = GlobalConn.Exec("INSERT INTO statistics VALUES("+strconv.Itoa(DataBaseSB.Users.ID)+", 1, 0)");
	    if GlobalErr != nil {
		panic(GlobalErr)
	    }
	    stGameID := strconv.Itoa(DataBaseSB.Gamestate.ID)
	    stSecret := SecretToString(FSData.SecretField)
	    _, GlobalErr = GlobalConn.Exec("INSERT INTO gamestate VALUES("+stGameID+", "+strconv.Itoa(DataBaseSB.Users.ID)+", 0, \""+stSecret+"\", \"\")");
	    if GlobalErr != nil {
		panic(GlobalErr)
	    }
	    _, GlobalErr = GlobalConn.Exec("INSERT INTO statistics VALUES("+strconv.Itoa(DataBaseSB.Users.ID)+", 1, 0");
	}
    }
    router := gin.Default()
    router.GET("/map", getMap)
    router.POST("/shoot", postShoot)
    router.GET("/count", getCount)
    router.GET("/stats", getStats)
    router.GET("/secret", getSecret)

    router.Run("localhost:8080")
}
